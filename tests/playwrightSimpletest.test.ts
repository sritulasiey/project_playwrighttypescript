import { test, expect } from '@playwright/test';

test('test @re', async ({ page }) => {
  await page.goto('https://www.google.com/');
  await page.getByRole('button', { name: 'Google Search' }).click();
  await page.frameLocator('iframe[name="callout"]').getByLabel('Stay signed out').click();
  await page.getByRole('button', { name: 'Google Search' }).click();
  await page.getByLabel('Google apps').click();
  await page.frameLocator('iframe[name="app"]').locator('.qWuU9c').click();
  await page.goto('https://www.google.com/maps');
  await page.getByRole('textbox', { name: 'Search Google Maps' }).click();
  await page.getByRole('textbox', { name: 'Search Google Maps' }).fill('bagamane');
  await page.getByRole('gridcell', { name: 'Bagmane Constellation Business Park' }).click();
  await expect(page).toHaveTitle('Bagmane Constellation Business Park - Google Maps');
  await page.waitForLoadState();

});